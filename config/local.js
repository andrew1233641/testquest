const config = {
    server: {
      port: process.env.PORT || 305,
      host: process.env.HOST || 'localhost',
      apiVersion: 'v1',
    }
  };
  
  module.exports = config;