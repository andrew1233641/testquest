(async () => {
    const Inert = require('@hapi/inert');
    const Vision = require('@hapi/vision');
    const HapiSwagger = require('hapi-swagger');
    const routes = require("./routes")
    const config = require("./config/local")
    const Hapi = require("@hapi/hapi")
    const NodeCache = require('node-cache');
    const cache = new NodeCache({ stdTTL: 60, checkperiod: 120 });

    
    const server = Hapi.server({
      port: config.server.port,
      host: config.server.host
    });
    
    process.on('uncaughtException', function (err) {
      console.error('Unhandled Exception', err);
      
    });
    
    process.on('uncaughtRejection', function (err) {
      console.error('Unhandled Exception', err);
    });
  
    await server.register([
        Inert,
        Vision,
        {
          plugin: HapiSwagger,
          options: {
            info: {
              title: 'API Documentation',
              version: config.server.apiVersion,
            },
          },
        }
      ])
    server.route(routes(server,cache))

    await server.start();
    
})();