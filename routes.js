const axios = require("axios");


const getRoutes = (server,cache) => {
  return [
    {
      method: 'GET',
      path: '/item/{id}',
      options: {
        handler: async (request, h) => {
          let result;
          try{
            //get remote from id
            result = await axios.get('https://jsonplaceholder.typicode.com/posts?id=' + request.params.id)
            //save
            cache.set(result,request.path + request.request.params.id);
          }catch(err){
            //get from id if remote unavailable
            result = cache.get(request.path + request.request.params.id);
          }
          return result;
        },
        description: 'Array properties',
        tags: ['api', 'petstore']
      }
    },
    {
      method: 'GET',
      path: '/item',
      options: {
        handler: async (request, h) => {
          let result;
          try{
            //get from remote
            result = await axios.get('https://jsonplaceholder.typicode.com/posts')
            //save in cache
            result?.forEach(element => {
              cache.set(element,request.path + element.id);
            }) 
          }catch(err){
            //get all if remote unavailable
            result = cache.get();
          }
          return result;
        },
        description: 'Array properties',
        tags: ['api', 'petstore']
      }
    },
    {
      method: 'POST',
      path: '/item',
      options: {
        handler: async (request, h) => {
          //post
          await axios.post('https://jsonplaceholder.typicode.com/posts', postData)
          let result = [];
          //save all postings
          postData.forEach(element => {
            cache.set(element,element.id);
            result.push(element.id);
          }); 
          //return array of saved ids
          return result;
        }
      }
    },
  ];
}

module.exports = getRoutes;